//
//  AnimationManager.h
//  MyBadGameFrameWork
//
//  Created by tomliang on 15/6/15.
//
//

#ifndef MyBadGameFrameWork_AnimationManager_h
#define MyBadGameFrameWork_AnimationManager_h

#include "BaseHeader.h"

class AnimationManager : public Singleton<AnimationManager>
{
public:
    AnimationManager(void);
    ~AnimationManager(void);
    //初始化动画模版缓存表
    bool initAnimationMap();
    //根据名字得到一个动画模板
    CCAnimation* getAnimation(int key);
    //创建一个动画实例
    CCAnimate* createAnimate(int key);
    //创建一个动画实例
    CCAnimate* createAnimate(const char* key);
    CCSpriteFrame * getSpritFrame(int key);
    
protected:
    /*加载各种动画
     CCAnimation* createXXXAnimation();
     
     */
};

//定义动画管理器实例的别名
#define sAnimationMgr AnimationManager::instance()

#endif
