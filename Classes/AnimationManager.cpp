//
//  AnimationManager.cpp
//  MyBadGameFrameWork
//
//  Created by tomliang on 15/6/15.
//
//

#include "AnimationManager.h"

DECLARE_SINGLETON_MEMBER(AnimationManager);
AnimationManager::AnimationManager(void)
{
}


AnimationManager::~AnimationManager(void)
{
}
bool AnimationManager::initAnimationMap()
{
    char temp[20];
    /*初始化动画
     
     */
    
    return true;
}
CCSpriteFrame * AnimationManager::getSpritFrame(int key)
{
    char temp[20];
    sprintf(temp, "%d", key);
    return CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(temp);
}

//获取指定动画模版
CCAnimation* AnimationManager::getAnimation(int key)
{
    char temp[20];
    sprintf(temp, "%d", key);
    return CCAnimationCache::sharedAnimationCache()->animationByName(temp);
}

//获取一个指定动画模版的实例
CCAnimate* AnimationManager::createAnimate(int key)
{
    //获取指定动画模版
    CCAnimation* anim = getAnimation(key);
    if(anim)
    {
        //根据动画模版生成一个动画实例
        return CCAnimate::create(anim);
    }
    return NULL;
}

//获取一个指定动画模版的实例
CCAnimate* AnimationManager::createAnimate(const char* key)
{
    //获取指定动画模版
    CCAnimation* anim = CCAnimationCache::sharedAnimationCache()->animationByName(key);
    if(anim)
    {
        //根据动画模版生成一个动画实例
        return CCAnimate::create(anim);
    }
    return NULL;
}
