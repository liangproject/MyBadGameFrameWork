//
//  BaseLayer.h
//  MyBadGameFrameWork
//
//  Created by tomliang on 15/6/15.
//
//

#ifndef MyBadGameFrameWork_BaseLayer_h
#define MyBadGameFrameWork_BaseLayer_h

#include "BaseHeader.h"

class BaseLayer : public cocos2d::Layer{

public:
    //初始化；
    virtual bool init();
    //创建；
    CREATE_FUNC(BaseLayer);
    //显示UI；
    void showUI();
    //读取数据；
    void GetReadData(void* data);
    //加载逻辑；
    void LoadUILogic(cocos2d::Ref* pSender, cocos2d::SEL_CallFuncND method);
    //返回逻辑；
    void ReturnLogic(int classIndex);
    
public:
    //UI管理器；
    cocos2d::Ref* m_pUIManager;
    //指定函数返回；
    cocos2d::SEL_CallFuncND m_UILoic;
};

#endif
